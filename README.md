Serenity Spa | Soul Yoga is a temple with the intent to provide healers, teachers, light workers, and community a place to gather, connect, & transform. Visit us at our Roseville or Folsom location.

Address: 3984 Douglas Blvd, #150, Roseville, CA 95661, USA

Phone: 916-797-8550
